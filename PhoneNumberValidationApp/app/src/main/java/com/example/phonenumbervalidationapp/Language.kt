package com.example.phonenumbervalidationapp

data class Language(val code:String, val name:String)
