package com.example.phonenumbervalidationapp

data class Detection(val language:String, val isReliable:Boolean, val confidence:Double)
